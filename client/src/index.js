// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { Card, Row, Column, Form, Button } from './widgets';
import taskService, { type Task } from './task-service';

class TaskList extends Component {
  tasks: Task[] = [];

  render() {
    return (
      <Card title="Tasks">

        <div>
          {this.tasks.map(task => (
            <div key={task.id}


              onClick={() => {
                taskService.uppdate(task.id, task.done).then(() => {
                  // Reloads the tasks in the Tasks component
                  TaskList.instance()?.mounted(); // .? meaning: call Tasks.instance().mounted() if Tasks.instance() does not return null
                  this.done = !this.done;
                });
              }}
            >

              <font color={task.done ? 'green' : 'black'}>
                <Row><Column>{task.title}</Column><Column>
                  <Button.Danger type="button"
                    onClick={() => {
                      taskService.delete(task.id).then(() => {
                        // Reloads the tasks in the Tasks component
                        TaskList.instance()?.mounted(); // .? meaning: call Tasks.instance().mounted() if Tasks.instance() does not return null
                        this.id = '';
                      });
                    }}> X </Button.Danger></Column></Row>
              </font>
            </div>
          ))}
        </div>

      </Card >
    );
  }

  mounted() {
    taskService.getAll()
      .then((tasks) => (this.tasks = tasks));
    //taskService.post(task.id).then((task) => (this.task.done = !this.task.done));
  }
}

class TaskNew extends Component {
  title = '';

  render() {
    return (
      <Card title="New task">
        <Row>
          <Column width={1}>
            <Form.Label>Title:</Form.Label>
          </Column>
          <Column width={4}>
            <Form.Input
              type="text"
              value={this.title}
              onChange={(event) => (this.title = event.currentTarget.value)}
            />
          </Column>
        </Row>
        <Button.Success
          onClick={() => {
            taskService.create(this.title).then(() => {
              // Reloads the tasks in the Tasks component
              TaskList.instance()?.mounted(); // .? meaning: call Tasks.instance().mounted() if Tasks.instance() does not return null
              this.title = '';
            });
          }}
        >
          Create
        </Button.Success>
      </Card>
    );
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <>
      <TaskList />
      <TaskNew />
    </>,
    root
  );

